<?php

use Drush\Log\LogLevel;

/**
 * Implements hook_drush_command().
 */
function drush_switch_drush_command() {
  $commands = array();

  $commands['switch'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_NONE,
    'description' => dt('Switch prod/dev environment.'),
    'arguments' => array(
      'environment' => dt('Environment type.'),
    ),
    'required-arguments' => TRUE,
    'examples' => array(
      'drush switch dev' => dt('Switch to dev environment.'),
    ),
  );

  return $commands;
}

/**
 * Implements drush_COMMAND().
 */
function drush_drush_switch_switch($environment) {
  if (!in_array($environment, array('prod', 'dev'))) {
    drush_die(dt('Environment should be "prod" or "dev". "@extension" given.', array('@extension' => $environment)));
  }
  // Path to "settings.php".
  $settings_php_path = 'sites/default/settings.php';
  // Read the "settings.php" file.
  $settings_php = file_get_contents($settings_php_path);

  // Code for "development" environment.
  $dev_code = "# if (file_exists(__DIR__ . '/settings.local.php')) {" . PHP_EOL;
  $dev_code .= "#   include __DIR__ . '/settings.local.php';" . PHP_EOL;
  $dev_code .= "# }" . PHP_EOL;

  // Code for "production" environment.
  $prod_code = "if (file_exists(__DIR__ . '/settings.local.php')) {" . PHP_EOL;
  $prod_code .= "  include __DIR__ . '/settings.local.php';" . PHP_EOL;
  $prod_code .= "}" . PHP_EOL;

  // Change file permission to update the file.
  chmod($settings_php_path, 0644);

  if ($environment == 'dev') {
    $settings_php = str_replace($dev_code, $prod_code, $settings_php, $count);
    if ($count === 1) {
      file_put_contents($settings_php_path, $settings_php);
      drush_log(dt('Environment switched to "development".'), LogLevel::SUCCESS);
    }
    else {
      drush_log(dt('Already on "development" environment.'), LogLevel::WARNING);
    }

  }
  else {
    $settings_php = str_replace($prod_code, $dev_code, $settings_php, $count);
    if ($count === 1) {
      file_put_contents($settings_php_path, $settings_php);
      drush_log(dt('Environment switched to "production".'), LogLevel::SUCCESS);
    }
    else {
      drush_log(dt('Already on "production" environment.'), LogLevel::WARNING);
    }
  }

  // Restore old file permissions.
  chmod($settings_php_path, 0444);

}
